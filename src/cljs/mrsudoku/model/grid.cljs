(ns mrsudoku.model.grid
  (:require [clojure.string :as string]
            [mrsudoku.utils :refer [concatv]]))

(defn mk-cell
  "Create a new cell."
  ([] {:status :empty})
  ([v] {:status :init, :value v})
  ([st v] {:status st, :value v}))

(defn cell-value [cell]
  (case (:status cell)
    (:init :set :solved :conflict) (:value cell)
    nil))

(def ^:private sudoku-grid
  [[;; row 1
    [(mk-cell 5) (mk-cell 3) (mk-cell)
     (mk-cell 6) (mk-cell) (mk-cell)
     (mk-cell) (mk-cell 9) (mk-cell 8)]
    [(mk-cell) (mk-cell 7) (mk-cell)
     (mk-cell 1) (mk-cell 9) (mk-cell 5)
     (mk-cell) (mk-cell) (mk-cell)]
    [(mk-cell) (mk-cell) (mk-cell)
     (mk-cell) (mk-cell) (mk-cell)
     (mk-cell) (mk-cell 6) (mk-cell)] ],
   [;; row 2
    [(mk-cell 8) (mk-cell) (mk-cell)
     (mk-cell 4) (mk-cell) (mk-cell)
     (mk-cell 7) (mk-cell) (mk-cell)]
    [(mk-cell) (mk-cell 6) (mk-cell)
     (mk-cell 8) (mk-cell) (mk-cell 3)
     (mk-cell) (mk-cell 2) (mk-cell)]
    [(mk-cell) (mk-cell) (mk-cell 3)
     (mk-cell) (mk-cell) (mk-cell 1)
     (mk-cell) (mk-cell) (mk-cell 6)]],
   [;; row 3
    [(mk-cell) (mk-cell 6) (mk-cell)
      (mk-cell) (mk-cell) (mk-cell)
     (mk-cell) (mk-cell) (mk-cell)]
    [(mk-cell) (mk-cell) (mk-cell)
     (mk-cell 4) (mk-cell 1) (mk-cell 9)
     (mk-cell) (mk-cell 8) (mk-cell)]
    [(mk-cell 2) (mk-cell 8) (mk-cell)
     (mk-cell) (mk-cell) (mk-cell 5)
     (mk-cell) (mk-cell 7) (mk-cell 9)]]])


(defn cell
  "Get the cell at coordinates `(cx,cy)`
with `cx` the column number and `cy` the row number."
  [grid cx cy]
  {:pre [(<= 1 cx 9)
         (<= 1 cy 9)]}
  (let [block-x (quot (- cx 1) 3)
        cell-x (rem (- cx 1) 3)
        block-y (quot (- cy 1) 3)
        cell-y (rem (- cy 1) 3)
        cell-ref (+ (* cell-y 3) cell-x)]
    ;(println (str "block-x=" block-x ", block-y=" block-y))
    ;(println (str "cell-x=" cell-x ", cell-y=" cell-y))
    (let [block ((grid block-y) block-x)]
      ;(println (str "block=" block))
      (block cell-ref))))

(cell sudoku-grid 1 4)

(defn change-cell
  "Change the `grid` cell at coordinates `(cx,cy)`
  with `cx` the column number and `cy` the row number,
  and `cell` is the new cell-content."
  [grid cx cy cell]
  {:pre  [(<= 1 cx 9)
          (<= 1 cy 9)]}
  (let [block-x (quot (- cx 1) 3)
        cell-x (rem (- cx 1) 3)
        block-y (quot (- cy 1) 3)
        cell-y (rem (- cy 1) 3)
        cell-ref (+ (* cell-y 3) cell-x)]
    (update-in grid [block-y block-x cell-ref] (fn [_] cell))))

(defn cell->str [cell]
  (condp = (:status cell)
    :init (str " " (:value cell) " ")
    :empty " . "
    :set (str "[" (:value cell) "]")
    :conflict (str "!" (:value cell) "!")))

(defn block
  "Get the #`b` block of the `grid`."
  [grid b]
  {:pre [(<= 1 b 9)]}
  (let [row (quot (- b 1) 3)
        blk (rem (- b 1) 3)]
    (nth (nth grid row) blk)))

(defn reduce-block
  "Reduce the `b`-th `block` of a sudoku grid with a function taking 4
  parameters: `acc` for the accumulated value, `index` for the cell
  index in the block, `cx, cy` for the cell
  coordinates in the grid and `cell` for the cell content."
  [f init block b]
  (let [bx (* 3 (rem (- b 1) 3))
        by (* 3 (quot (- b 1) 3))]
    (loop [yoffset 0, index 1, cells block, acc init]
      (if (= yoffset 3)
        acc
        (let [[nacc, ncells, nindex]
              (loop [xoffset 0, index index, cells cells, acc acc]
                (if (= xoffset 3)
                  [acc, cells, index]
                  (recur (+ xoffset 1) (+ index 1)
                         (rest cells) (f acc
                                         index
                                         (+ bx xoffset 1)
                                         (+ by yoffset 1)
                                         (first cells)))))]
          (recur (+ yoffset 1) nindex ncells nacc))))))


(defn do-block
  "Do the effects in function `f!` while traversing the `b`-th `block`
  of a sudoku grid . The function `f!` takes 4 parameters: `index` for
  the index of the cell in the block, `cx, cy`
  for the cell coordinates in the grid and `cell` for the cell content."
  [f! block b]
  (reduce-block (fn [_ index cx cy cell]
                  (f! index cx cy cell)) nil block b))


(defn block-row
  "Get the #`r` row in the `grid`."
  [block r]
  {:pre [(<= 1 r 3)]}
  (let [base (* (- r 1) 3)]
    [(nth block base) (nth block (+ base 1)) (nth block (+ base 2))]))

(defn row
  "Get the #`r` row in the `grid`."
  [grid r]
  {:pre [(<= 1 r 9)]}
  (let [block-r (quot (- r 1) 3)
        row-r (+ (rem (- r 1) 3) 1)
        blocks (nth grid block-r)]
    (concatv (block-row (nth blocks 0) row-r)
             (block-row (nth blocks 1) row-r)
             (block-row (nth blocks 2) row-r))))

(defn row->str [row]
  (string/join " " (map cell->str row)))

(defn rows
  "Fetch all the rows of a sudoku `grid`."
  [grid]
  (for [i (range 1 10)]
    (row grid i)))

(defn grid->str [grid]
  (string/join "\n" (map row->str (rows grid))))

(defn col
  "Get the #`c` column of the `grid`."
  [grid c]
  {:pre [(<= 1 c 9)]}
  (into [] (for [cy (range 1 10)]
             (cell grid c cy))))

(defn reduce-grid
  "Reduce the whole `grid` of a sudoku with a function taking 4
  parameters: `acc` for the accumulated value, `cx, cy` for the cell
  coordinates and `cell` for the cell content."
  [f init grid]
  (loop [cy 1, acc init]
    (if (= cy 10)
      acc
      (let [nacc (loop [cx 1, acc acc]
                   (if (= cx 10)
                     acc
                     (recur (+ cx 1) (f acc cx cy (cell grid cx cy)))))]
        (recur (+ cy 1) nacc)))))

(defn do-grid
  "Do the effects in function `f!` while traversing the `grid` of a
  sudoku. The function `f!` takes 3 parameters: `cx, cy` for the cell
  coordinates and `cell` for the cell content."
  [f! grid]
  (reduce-grid (fn [_ cx cy cell]
                 (f! cx cy cell)) nil grid))

(reduce-grid (fn [acc cx cy cell]
               (conj acc (vector [cx cy] cell)))
            {} sudoku-grid)

;; ============================
;; PARTIE DE conflict.cljs
;; ============================

(defn values
  "Return the set of values of a vector or grid `cells`."
  [cells]
  ;; Attention : réponse fausse
  (loop [s cells, res #{}]
    (if (seq s)
      (if (= (get (first s) :value) nil)
        (recur (rest s) res)
        (recur (rest s) (conj res (get (first s) :value))))
      res)))

(values (col sudoku-grid 1))

(defn values-except
  "Return the set of values of a vector of cells, except the `except`-th."
  [cells except]
  {:pre [(<= 1 except (count cells))]}
  (loop [s cells, c 1, res []]
    (if (seq s)
      (if (or (= (get (first s) :value) nil) (= c except))
        (recur (rest s) (+ c 1) res)
        (recur (rest s) (+ c 1) (conj res (get (first s) :value))))
      (into #{} res))))

(values-except (block sudoku-grid 1) 4)


(defn mk-conflict [kind cx cy value]
  {:status :conflict
   :kind kind
   :value value})

(defn merge-conflict-kind
  [kind1 kind2]
  (if (coll? kind1)
    (if (coll? kind2)
      (loop [s kind1, res kind2]
        (if (seq s)
          (recur (rest s) (conj res (first s)))
          res))
      (conj kind1 kind2))
    (if (coll? kind2)
      (conj kind2 kind1)
      (if (= kind1 kind2)
        kind1
        (hash-set kind1 kind2)))))

(defn merge-conflict [conflict1 conflict2]
  (conj conflict1 conflict2))

(defn merge-conflicts [& conflicts]
  (apply (partial merge-with merge-conflict) conflicts))

(defn update-conflicts
  [conflict-kind cx cy value conflicts]
  (if-let [conflict (get conflicts [cx, cy])]
    (assoc conflicts [cx, cy] (mk-conflict (merge-conflict-kind conflict-kind (:kind conflict))
                                           cx cy value))
    (assoc conflicts [cx, cy] (mk-conflict conflict-kind cx cy value))))

(defn conflict-value [values except cell]
  (when-let [value (cell-value cell)]
    (when (and (not= (:status cell) :init)
               (contains? (values-except values except) value))
      value)))

(defn col-conflicts
  "Returns a map of conflicts in a `col`."
  [col cx]
  (loop [cy 1, cells (seq col), conflicts {}]
    ;(println conflicts)
    (if (seq cells)
      (let [cell (first cells)]
        (if-let [value (conflict-value col cy cell)]
          (recur (+ cy 1) (rest cells) (update-conflicts :col cx cy value conflicts))
          (recur (+ cy 1) (rest cells) conflicts)))
      ;; no more cells
      conflicts)))

(defn cols-conflicts
  [grid] (reduce merge-conflicts {}
                 (map (fn [c] (col-conflicts (col grid c) c)) (range 1 10))))


(defn row-conflicts
  "Returns a map of conflicts in a `row`."
  [row cy]
  (loop [s (seq row), i 1, res {}]
    (if (seq s)
      (let [cell (first s)]
        (if-let [value (conflict-value row i cell)]
          (recur (rest s) (+ i 1) (update-conflicts :row i cy value res))
          (recur (rest s) (+ i 1) res)))
      res)))


(defn rows-conflicts
  "Returns a map of conflicts in all rows of `grid`"
  [grid]
  [grid] (reduce merge-conflicts {}
                 (map (fn [c] (row-conflicts (row grid c) c)) (range 1 10))))

(row-conflicts (map #(mk-cell :set %) [1 2 3 nil nil 1]) 1)

(row-conflicts (map #(mk-cell :set %) [1 2 3 1]) 1)

(defn block-conflicts
  [block b]
  (reduce-block (fn [conflicts index cx cy cell]
                    (if-let [value (conflict-value block index cell)]
                      (update-conflicts :block cx cy value conflicts)
                      conflicts)) {} block b))

(defn blocks-conflicts
  [grid]
  (reduce merge-conflicts {}
          (map (fn [b] (block-conflicts (block grid b) b)) (range 1 10))))

(defn grid-conflicts
  "Compute all conflicts in the Sudoku grid."
  [grid]
  (println "compute conflicts")
  (println (grid->str grid))
  (merge-conflicts (rows-conflicts grid)
                   (cols-conflicts grid)
                   (blocks-conflicts grid)))

;; ===========================
;; Fonctions données en cours
;; ===========================

;; ===========================
;; Outils : ajoute 1 arete
;;          enleve 1 arete
;; ===========================

(defn add_edge
  [graph src dest]
  (update graph src #(conj % dest)))

(add_edge  {:x1 #{1 4 43} :x2 #{4} :x3 #{43}} :x1 3)

(defn remove_edge
  [graph src dest]
  (update graph src #(disj % dest)))

(remove_edge  {:x1 #{1 4 43} :x2 #{4} :x3 #{43}} :x1 4)

(defn inv-edges
  [src dest]
  (zipmap dest (repeat #{src})))

(inv-edges :a #{:b :c :d})

;; ====================================
;; Fonctions : augment et max-matching
;; ====================================

(defn augment
  [graph src visited match]
  (loop [dests (get graph src), visited visited ]
    (if (seq dests)
      (if-let [old-src (get match (first dests))]
        (let [[found, visited', match'](augment old-src (conj visited (first dests) match))]
          (if found
            [true, visited', (assoc match' (first dests) src)]
            (recur (rest dests) visited')))
      [true, (conj visited (first dests)) (assoc match (first dests) src) ])
  [false, visited, match])))

(augment {:x2 #{4} :x3 #{4}} :x2 #{} {})

(defn max-matching
  [graph]
  (loop [i (count graph), gr graph, visited #{}, res {}]
      (let [[kmap vmap] (first gr)
            [a, visSet, matchMap] (augment graph kmap visited res)]
        (if (> i 0)
          (recur (dec i) (rest gr) (into visited visSet) (merge res matchMap))
          res))))

(max-matching {:x1 #{1 2 3} :x2 #{43} :x3 #{4}})

;; ====================================
;; Fonctions finales
;; ====================================

(defn isolated-values
  [variables sec]
  (into {}( map (first filter #(and (=(count %) 1) (not (variables (first %))))) sec)))

(defn value-known-by
  [doms value]
  (reduce (fn[res [v values]]
            (if (contains? values value)
              (conj res v)
              res)) #{} doms))

(value-known-by {:x1 #{1 2 3} :x2 #{1 43}} 1)

(defn add-values
  [doms vs value]
  (into doms (map (fn[var]
                  [var (conj (get doms var) value)])
                  vs)))

(defn acces
  [doms scc]
  (let [scc-doms (doms-from-scc (vars-of doms) scc)
        isolated (isolated-values (vars-of doms) scc)]
        (reduce (fn [doms' value]
                    (add-value doms (value-known-by doms value) value))
                  scc-doms isolated)))

(defn alldiff
  [doms]
  (let [match (max-matching doms)]
    (if complete-matching? doms match)
      (let [scc (compute_scc(graph-with-matching doms match))]
          acces doms scc))
          ;;incomplet
          nil )