// Compiled by ClojureScript 1.10.339 {}
goog.provide('mrsudoku.model.solver');
goog.require('cljs.core');
goog.require('cljs.test');
goog.require('mrsudoku.model.grid');
goog.require('mrsudoku.model.conflict');
/**
 * Solve the sudoku `grid` by returing a full solved grid,
 *  or `nil` if the solver fails.
 */
mrsudoku.model.solver.solve = (function mrsudoku$model$solver$solve(grid){
if(cljs.core._EQ_.call(null,mrsudoku.model.conflict.grid_conflicts.call(null,grid),cljs.core.PersistentArrayMap.EMPTY)){
return null;
} else {
return null;
}
});

//# sourceMappingURL=solver.js.map
