(ns mrsudoku.model.solver
  (:require [cljs.test :refer-macros [deftest testing is]]
          [mrsudoku.model.grid :as g]
          [mrsudoku.model.conflict :as c]))

(defn solve
  "Solve the sudoku `grid` by returing a full solved grid,
 or `nil` if the solver fails."
  [grid]
  (if (= (c/grid-conflicts grid) {})

    nil))
